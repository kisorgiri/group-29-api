const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    point: {
        type: String,
        min: 1,
        max: 5
    },
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: String,
    description: String,
    price: Number,
    brand: String,
    quantity: Number,
    color: String,
    category: {
        type: String,
        required: true
    },
    manuDate: Date,
    expirtyDate: Date,
    warrentyStatus: Boolean,
    warrentyPeroid: String,
    weight: String,
    size: String,
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['quantity', 'percentage', 'value']
        },
        discountValue: String
    },
    offers: [String],
    tags: [String],
    reviews: [ReviewSchema],
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    images: [String]

}, {
    timestamps: true
})

module.exports = mongoose.model('product', ProductSchema);;