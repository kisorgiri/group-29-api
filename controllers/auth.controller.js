const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader');
const JWT = require('jsonwebtoken');
const passwordHash = require('password-hash');
const config = require('./../configs');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function prepareMail(data) {
    let mailBody = {
        from: 'Group29 Store', // sender address
        to: "pakariunsworth@gmail.com,prattap18@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `
        <p>Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system. please use link below to reset your password</p>
        <p>
        <a href="${data.link}">Click here to reset password</a>
        </p>
        <p>If you have not requested for password reset kindly ignore this email</p>
        <p>Regards,</p>
        <p>Group 29 store support team</p>`, // html body
    }
    return mailBody;
}

function getToken(data) {
    let token = JWT.sign({
        name: data.username,
        role: data.role,
        _id: data._id
    }, config.JWTSecret);

    return token;
}

router.get('/', function (req, res, next) {
    require('fs').readFile('./klsdjf.sdklfj', function (err, data) {
        if (err) {
            return req.myEvent.emit('error', err, res);
        }
    })
})

router.post('/login', function (req, res, next) {
    UserModel
        .findOne({
            $or: [
                { username: req.body.username },
                { email: req.body.username }
            ]
        })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'Invalid Username',
                    status: 400
                })
            }
            var isMatched = passwordHash.verify(req.body.password, user.password);
            if (isMatched) {
                // token generation
                var token = getToken(user);
                res.json({
                    user: user,
                    token: token
                });
            } else {
                next({
                    msg: "Invalid Password",
                    status: 400
                })
            }
        })
})

router.post('/register', uploader.single('img'), function (req, res, next) {
    console.log('req.body >>', req.body);
    console.log('req.file >>', req.file)
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }

    const data = req.body;
    if (req.file) {
        data.image = req.file.filename
    }
    // db stuff
    const newUser = new UserModel({}); // mongoose's object
    const newMappedUser = MapUser(newUser, req.body);

    // newUser.save(function (err, done) {
    //     if (err) {
    //         return next(err);
    //     }
    //     res.json(done);
    // })
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser
        .save()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/forgot-password', function (req, res, next) {
    console.log('req.body>>', req.body)

    UserModel
        .findOne({
            email: req.body.email
        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Email not registered yet",
                    status: 400
                })
            }
            var mailData = {
                name: user.username,
                email: user.email,
                link: `${req.headers.origin}/reset_password/${user._id}`
            }
            var email = prepareMail(mailData);

            const expiryTime = 1000 * 60 * 60 * 24;

            user.passwordResetExpiry = new Date().getTime() + expiryTime;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(email, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
            })

        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/reset-password/:id', function (req, res, next) {
    UserModel.findOne({
        _id: req.params.id,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    },
        function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Password reset link expired",
                    status: 404
                })
            }
            // if (new Date(user.passwordResetExpiry).getTime() < Date.now()) {
            //     return next({
            //         msg: "Password reset link expired",
            //         status: 400
            //     })
            // }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetExpiry = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
})


module.exports = router;