const express = require('express');
const path = require('path');
const app = express();
const cors = require('cors');
app.set('port', 9090)


// event stuff
const events = require('events');
const myEvent = new events.EventEmitter();

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

myEvent.on('error', function (err, res) {
    console.log('on error event >>', err)
    res.status(400);
    res.json(err);
})


// templating engine setup
const pug = require('pug')
app.set('view engine', pug);
app.set('views', path.join(__dirname, 'views'));

require('./db');
require('./socket')(app);


const morgan = require('morgan');

// import api routes
const APIRoute = require('./routes/api.route');
// load thirdparty middleware
app.use(morgan('dev'));
app.use(cors())

// inbuilt middleware
app.use(express.static('uploads')); // internal serving of static files
app.use('/file', express.static(path.join(__dirname, 'uploads')));

// incoming data must be parsed to use in express
// step to parse data
// find the contetnt type
// use appropriate parser
// this is parser for x-www-form-urlecoded content type
app.use(express.urlencoded({
    extended: true
}));
// this middleware will add a new body property in req with parsed value
app.use(express.json()); // json parser

// console.log('__dirname in main file >>', __dirname);

// load routing level middleware
app.use('/api', APIRoute);

// application level middleware acting as 404 handler
app.use(function (req, res, next) {
    next({
        msg: "Not Found",
        status: 404
    })
})


// error handling middleware
// to call error handling middleware one must call next with argument
app.use(function (err, req, res, next) {
    console.log('error is >>', err);
    // first argument is for error
    // req,res,next is same as it is of application level middleware
    // todo ALWAYS add status code in response
    res.status(err.status || 400)
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})


app.listen(app.get('port'), function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + app.get('port'))
    }
})

// fs.rename(oldName(path), newName(path),cb)
// remove
// fs.unlink(path_to_file,cb)


// middleware
// function(){
// closure,
// calback,HOF,functional constructor,method
// }
// middleware
// middleware is a function that came into action between request response cycle
// middleware has access to http request object, http response object and next middleware function reference
// middleware can modify http request object and response

// syntax
// function(req,res,next){
//     // req or 1st argument is http request object
//     // res or 2nd argument is http response object
//     // next or 3rd argument is next middleware function reference
// }

// configuration block
// app.use(middleware)  .use is configuration for middleware


// the order of middleware is very important

// there are 5 types of middleware
// 1. application level middleware
// if we have access of req,res,next then it is application level middleware
// 2. routing level middleware //
// 3.third party middleware
// 4.error handling middleware
// 5.inbuilt middleware
