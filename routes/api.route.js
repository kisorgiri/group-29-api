const router = require('express').Router();
const AuthRouter = require('./../controllers/auth.controller');
const UserRouter = require('./../controllers/user.controller');
const ProductRouter = require('./../module/products/product.route');
const Authenticate = require('./../middlewares/Authenticate');

router.use('/auth', AuthRouter);
router.use('/user', Authenticate, UserRouter)
router.use('/product', ProductRouter)



module.exports = router;