const JWT = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    let token;
    if (req.headers['authorization'])
        token = req.headers['authorization']
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.headers['token'])
        token = req.headers['token']
    if (req.query.token)
        token = req.query.token;

    if (token) {
        JWT.verify(token, config.JWTSecret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            UserModel.findById(decoded._id, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: "User Removed From system",
                        status: 400
                    })
                }
                req.loggedInUser = user;
                next(); 
            })
        })
    }
    else {
        next({
            msg: "Authentication Failed, Token not provided",
            status: 400
        })
    }
}