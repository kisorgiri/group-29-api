const ProductModel = require('./product.model');

function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name
    if (productDetails.description)
        product.description = productDetails.description
    if (productDetails.category)
        product.category = productDetails.category
    if (productDetails.price)
        product.price = productDetails.price
    if (productDetails.color)
        product.color = productDetails.color
    if (productDetails.brand)
        product.brand = productDetails.brand
    if (productDetails.quantity)
        product.quantity = productDetails.quantity
    if (productDetails.weight)
        product.weight = productDetails.weight
    if (productDetails.size)
        product.size = productDetails.size
    if (productDetails.tags)
        product.tags = typeof (productDetails.tags) === 'string' && productDetails.tags.split(',');
    if (productDetails.offers)
        product.offers = typeof (productDetails.offers) === 'string' && productDetails.offers.split(',')
    if (productDetails.vendor)
        product.vendor = productDetails.vendor
    if (productDetails.images)
        product.images = productDetails.images
    if (productDetails.warrentyStatus)
        product.warrentyStatus = productDetails.warrentyStatus
    if (productDetails.warrentyPeroid)
        product.warrentyPeroid = productDetails.warrentyPeroid
    if (productDetails.manuDate)
        product.manuDate = productDetails.manuDate
    if (productDetails.expiryDate)
        product.expiryDate = productDetails.expiryDate
    if (productDetails.discountedItem || productDetails.discountType || productDetails.discountValue) {
        if (!product.discount)
            product.discount = {};
        if (productDetails.discountedItem)
            product.discount.discountedItem = productDetails.discountedItem
        if (productDetails.discountType)
            product.discount.discountType = productDetails.discountType
        if (productDetails.discountValue)
            product.discount.discountValue = productDetails.discountValue
    }
    if (productDetails.reviewPoint && productDetails.reviewMessage) {
        // todo add user
        var review = {
            point: productDetails.reviewPoint,
            message: productDetails.reviewMessage,
            user: productDetails.user
        }
        product.reviews.push(review);
    }


}

function find(condition) {
    return ProductModel
        .find(condition)
        .populate('vendor', {
            username: 1,
            email: 1
        })
        .sort({
            _id: -1
        })
        .exec();
}

function save(data) {
    const newProduct = new ProductModel({});
    map_product_req(newProduct, data);
    return newProduct.save();

}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            var oldImage = product.images[0];
            // product found now update
            map_product_req(product, data);
            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve({
                    data: updated,
                    oldImage: oldImage
                });
            })
        })
    })

}

function remove(id) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            product.remove(function (err, removed) {
                if (err) {
                    return reject(err);
                }
                resolve(removed);
            })
        })
    })

}


module.exports = {
    find,
    save,
    update,
    remove,
    map_product_req
}