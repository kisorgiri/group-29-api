const router = require('express').Router();
const Uploader = require('./../../middlewares/uploader');
const Authenticate = require('./../../middlewares/Authenticate');

const ProductCtrl = require('./product.controller');

router.route('/')
    .get(Authenticate, ProductCtrl.get)
    .post(Authenticate, Uploader.array('img'), ProductCtrl.insert);

router.route('/search')
    .get(ProductCtrl.search)
    .post(ProductCtrl.search);

router.route('/:id')
    .get(Authenticate, ProductCtrl.getById)
    .put(Authenticate, Uploader.single('img'), ProductCtrl.update)
    .delete(Authenticate, ProductCtrl.remove);

module.exports = router;

