const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');
const router = require('express').Router();
const uploader = require('./../middlewares/uploader');

// console.log('__dirname  in user file', __dirname)
// console.log('-_-rot directory path >',process.cwd())


router.route('/')
    .get(function (req, res, next) {
        // projection
        // {
        //     // username: 1,
        //     role: 0
        // }
        UserModel
            .find({})
            .sort({
                _id: -1
            })
            // .limit(1)
            .exec(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
    })
    .post(function (req, res, next) {
        res.json({
            msg: 'from post of empty user'
        })
    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/search')
    .get(function (req, res, next) {
        // res.send('from user search request')
        require('fs').readFile('sdlkfj.dsklfj', function (err, done) {
            if (err) {
                return next(err);
            }
        })
    })

router.route('/:id')
    .get(function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            res.json(user);
        })
    })
    .put(uploader.single('img'), function (req, res, next) {

        if (req.fileTypeError) {
            return next({
                msg: "Invalid File Format",
                status: 400
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            // user exist now update


            const data = req.body;
            if (req.file) {
                data.image = req.file.filename
            }
            const mappedUpdatedUser = MapUser(user, data)

            mappedUpdatedUser.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })


        })

    })
    .delete(function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed)
            })
        })
    });




module.exports = router;