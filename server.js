const http = require('http');

const server = http.createServer(function (req, res) {
    // req or 1st argument is always http request object
    // res or 2nd argument is always http response object
    // regardless of http method and http url this callback block is executed
    console.log('client connected to server');
    console.log('request url >>', req.url);
    console.log('request method >>', req.method);

    // if(req.url ==='/write){

    // }

    // if(req.url ==='/read'){

    // }

    // req response cycle must be completed
    res.end('Welcome to Nodejs');

    // regardless of any http method and regardless of any http url this callback is executed
    // 
})

server.listen(9090, function (err, done) {
    if (err) {
        console.log('error listning >>', err);
    }
    else {
        console.log('server listening at port 9090 inside')
        console.log('press CTRL +C to exit from server')
    }
});