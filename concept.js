// Nodejs is JavaScript runtime environment
// NPM and NPX are package management tool which are installed when we install node

// we have command to work with npm

// npm init ==> this command is used is initialize javascript project
// it will create a pacakge.json file

// pacakge.json file is a project introductory file that holds the entire information of a project
// try to maintain updated information within pacakge.json file

// npmjs.com // it is a global repository that holds javascript related packages

// npm install <pacakge_name>
// it will install the package from npmjs to local porject folder and keep inside node_modules folder


// npm install 
// it will check pacakge.json file and install all the required dependencies

// npm uninstall<pacakge_name>


// nodejs ==> server side javascript
// file - file communication

// one file must export
// another file must import

// syntax
// es5
// export
// module.exports = j pani

// import syntax
// const abc = require('./path_to_file');

// whenever we import packages from node_modules or nodejs internal module
// whe should not give path
// const multer = require('multer');
// const path = require('path');


// REST API
// REST architecture
// following pointes needs to be fullfilled to be on REST
// 1. stateless
// 2.correct use of http verb
// GET ==>fetch
// PUT/PATCH==> update
// POST ==> insert
// DELETE ===>remove

// 3. data must be on either JSON or XML

// 4. we can cache GET request



// API ==> application programming interface

// 