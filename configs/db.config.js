const mongodb = require('mongodb');
const Mongoclient = mongodb.MongoClient;
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'group29db';
const OID = mongodb.ObjectID;

module.exports = {
    Mongoclient,
    conxnURL,
    dbName,
    OID
}