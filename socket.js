const socket = require('socket.io');
const config = require('./configs');

const users = [];
module.exports = function (app) {
    const io = socket(app.listen(config.socketPORT), {
        cors: {
            allow: '*/'
        }
    });
    io.on('connection', function (client) {
        var id = client.id;
        console.log('client connected to socket server >>');
        client.on('new-user', function (username) {
            users.push({
                id: id,
                name: username
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        client.on('new-msg', function (data) {
            client.emit('reply-msg-own', data); // jun client bata request aako tesailai matrai
            client.broadcast.to(data.receiverId).emit('reply-msg', data);// aafu bahek aru sabai client lai
        })

        client.on('disconnect', function () {
            users.forEach(function (user, index) {
                if (user.id === id) {
                    users.splice(index, 1);
                }
            })
            client.broadcast.emit('users', users);
        })
    })
}