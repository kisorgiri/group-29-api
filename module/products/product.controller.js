const ProductQuery = require('./product.query');
const fs = require('fs');
const path = require('path');

function get(req, res, next) {
    const condition = {}
    if (req.loggedInUser.role != 1) {
        condition.vendor = req.loggedInUser._id;
    }
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {
    const condition = { _id: req.params.id }
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function insert(req, res, next) {
    console.log('req.file >>', req.file);
    console.log('req.files >>', req.files);
    if (req.fileTypeError) {
        return next({
            msg: "Invalid file format",
            status: 400
        })
    }

    const data = req.body;
    if (req.file) {
        data.images = req.file.filename
    }
    if (req.files) {
        data.images = req.files.map(file => file.filename);
    }
    data.vendor = req.loggedInUser._id;
    // TODO add more information in data
    ProductQuery
        .save(data)
        .then(function (response) {
            res.json(response);
        })
        .catch(function (err) {
            next(err);
        })
}

function update(req, res, next) {

    if (req.fileTypeError) {
        return next({
            msg: "Invalid file format",
            status: 400
        })
    }

    const data = req.body;
    if (req.file) {
        data.images = req.file.filename
    }
    data.user = req.loggedInUser._id;
    if (Object.keys(data.vendor)) {
        data.vendor = data.vendor._id;
    }
    ProductQuery
        .update(req.params.id, data)
        .then(function (response) {
            if (req.file) {
                fs.unlink(path.join(process.cwd(), 'uploads/images/' + response.oldImage), function (err, removed) {
                    if (err) {
                        return console.log('failed', err);
                    }
                    console.log("removed");
                })
            }
            res.json(response.data);
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}
function search(req, res, next) {
    console.log('req.body>>>', req.body);
    const searchCondition = {}
    ProductQuery.map_product_req(searchCondition, req.body);

    console.log('search condition here >>', searchCondition);
    if (req.body.minPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice,
            $lte: req.body.maxPrice
        }
    }

    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);

        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    if (req.body.tags) {
        searchCondition.tags = {
            $in: req.body.tags.split(',')
        }
    }
    if (req.body.offers) {
        searchCondition.offers = {
            $in: req.body.offers.split(',')
        }
    }
    console.log('search condition >>', searchCondition);
    ProductQuery
        .find(searchCondition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    get,
    getById,
    insert,
    update,
    remove,
    search
}