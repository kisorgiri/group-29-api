const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    // db modelling
    name: String,
    address: {
        temp_address: [String],
        permanent_address: String
    },
    phoneNumber: {
        type: Number
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    status: {
        type: String,
        default: 'active'
    },
    dob: {
        type: Date
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    role: {
        type: Number, //1 for admin, 2 for end user
        default: 2
    },
    image: String,
    passwordResetExpiry: Date
}, {
    timestamps: true
})

const UserModel = new mongoose.model('user', UserSchema);

module.exports = UserModel;