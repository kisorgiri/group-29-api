// database is a container where data operation occurs

const e = require("express");

// database management system is way to use database
// there are two types of DBMS

// 1.Relational Database Management System (RDBMS)

// 2. Distirbuted Database Management System


// 1.RDBMS
// a.Table Based Design
// eg for LMS
// books, users,notifications are table
// b.tuple/row are single record inside table
// eg think and grow rich, ram,shyam
// c.schema based solution
// books:{
//     id:String,
//     author:user_id,
//     price:Number,
//     genre:string
// }
// d. non scalable
// e. sql database ==> Structured Query Language
// f. type => mysql,sqllite,mssql,
// g. relation between table exists

// 2.DDBMS
// a. collection based approach
// books, users, comments, reviews
// b. each record are called document
// document based database
// a document is valid JSON object
// c.schema less design
// d.higly scalable
// e.nosql database Not only SQL
// type==> mongodb,couch db, dynamadb,redis
// f. there is no any relation between collections

// mongodb

// mongodb programme must be installed and the srver programme must be running
// mongod(command to run the driver)

// commands
// mongo  ==> it will establish connection and gives us the > interface to work with shell command

// shell commands
// show dbs ==> list all the available databases
// use <db_name> if db exists the it will select exising database else it will create new database 

// db
// shows selected db

// show collections
// it will list all the available collections of the selected database

// database operation is refered as CRUD operation

// inserting document in collection
// CREATE
// db.<collection_name>.insert({valid_object});
// db.<collection_name>.insertMany([array of objects]);

// READ
// db.<collection_name>.find({query_selector})
// db.<collection_name>.find({query_selector}).count() // returns the overall count of document
// db.<collection_name>.find({query_selector}).pretty() to get formatted view


// UPDATE
// db.<collection_name>.update({},{},{})
// 1st object is for query selector
// 2nd object is another object followed by key as $set and value as object to be updated,
// 3rd object is optional where we add options multi, upsert

// DELETE
// db.<collection_name>.remove({query_selector})
// db.<collection_name>.remove({not kept it empty});

// to drop collection
// db.<collection_name>.drop();

// to drop database
// db.dropDatabase();


// mongoose
// ODM ==> Object Document modelling


// advantanges
// --1 Schema based solution
// 2 data type
// 3 method 
// 4indexing is lot more easier
// required, unique
// 5. middleware ==> 


// Database BACKUP & RESTORE
// JSON and CSV || BSON

// mongorestore, mangodump
// mongoimport mongoexport


//BSON 
// backup
// command mongodump
// mongodump ===> it will backup all the database inside default dump folder
// mongodump --db <db_name> it will backup selected database in default dump folder
// mongodump --db <db_name> --out <path_to_destination_folder>

// restore
// command mongorestore
// mongorestore ==> it will look for default dump folder and restore all the database
// mongorestore --drop ==> it will drop the existing document
// mongorestore path_to_source folder

// JSON
// backup
// command mongoexport
// mongoexport --db <db_name> --collection<col_name> --out <path_to_destination_with_.json_extension>
// mongoexport -d <db_name> -c <col_name> -o<path_to_destination_with_.json_extension>

// restore mongoimport
// mongoimport --db<existing_or_new_db> --collection<collection_name> path_to_source file


// CSV
// backup command mongoexport
// mongoexport --db<db_name> --collection<collection_name> --type=csv --fileds 'comma,seperated,valuefor columns of csv' --out <path_to_destination_with_.csv_extension>
// mongoexport --db<db_name> --collection<collection_name> --type=csv --query="{brand:'abc'}" --fileds 'comma,seperated,valuefor columns of csv' --out <path_to_destination_with_.csv_extension>

// import command ==> mongoimport
// mongoimport --db<db_name> --collection<collection_name> --type=csv path_to_source file with.csv --headerline



