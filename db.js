const mongoose = require('mongoose');

const dbConfig = require('./configs/db.config');

mongoose.connect(dbConfig.conxnURL + '/' + dbConfig.dbName, {
    useUnifiedTopology: true,
    useNewUrlParser: true
})
mongoose.connection.once('open', function () {
    console.log('db connection open');
})
mongoose.connection.on('err', function (err) {
    console.log('db connection failed')
})